# Importing Libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# Importing Datasets
dataset = pd.read_csv("Dataset.csv")
predictionset = pd.read_csv("Predictionset.csv")
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 20].values
predictionset = predictionset.iloc[:, 1:].values

# Splitting into Train & Test Sets
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X , y, test_size = 0.2, random_state = 0)

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
predictionset = sc_X.transform(predictionset)

# Applying Grid Search to find the best model and the best parameters
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
parameters = [{'C': [1, 10, 100, 1000], 'kernel': ['linear']},
              {'C': [1, 10, 100, 1000], 'kernel': ['rbf'], 'gamma': [0.5, 0.1, 0.01, 0.001, 0.0001]}]
grid_search = GridSearchCV(estimator = SVC(),
                           param_grid = parameters,
                           scoring = 'accuracy',
                           cv = 10,
                           n_jobs = -1)
grid_search = grid_search.fit(X_train, y_train)
best_accuracy = grid_search.best_score_
best_parameters = grid_search.best_params_

#Fitting the Classification Model to the Dataset (We get the Best Params as C = 100, Kernel = Linear)
classifier = SVC(C = 100, kernel = 'linear', random_state = 0)
classifier.fit(X_train, y_train)

# Applying k-Fold Cross Validation
from sklearn.model_selection import cross_val_score
accuracies = cross_val_score(estimator = classifier, X = X_train, y = y_train, cv = 10)
accuracies.mean()
accuracies.std()

# Predicting the Prediction Set
predictedset = classifier.predict(predictionset)

# Applying Inverse Feature Scaling
predictionset = sc_X.inverse_transform(predictionset)  

# Appending the y_test to X_test to make it one dataset
resultset = np.append(arr = predictionset, values = predictedset[:, None], axis = 1)